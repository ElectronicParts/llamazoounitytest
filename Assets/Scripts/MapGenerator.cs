﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour
{
	[SerializeField]
	int width, height;
	
	[Range (0, 100)]
	[SerializeField]
	int wallPercent;

	[Header ("Required")]
	public string name;

	[Header ("Optional")]
	[SerializeField]
	public string seed;

	//setting this to true by default will allow users to randomize their cave layouts without having to enter new seeds each time
	//if users wish, they can uncheck this and use their own custom seeds if they like
	[SerializeField]
	public bool randomizeMapSeed = true;

	int[,] map, newMap;

	//I set the number of smoothing cycles to 5 as thats what I've found produces the nicest caves, however feel free to play with this
	int smoothingCycles = 5;


	public void GenerateMap ()
	{
		//creating the initial map using the users width and height
		map = new int[width, height];
		//Populating the map based on the user's desired wall percent
		PopulateMap ();
		//smoothing the map out based on the number of desired smoothing cycles
		for (int i = 0; i < smoothingCycles; i++) {
			//making a copy of the current map
			newMap = map;
			//smoothing out the copy of the map
			SmoothMap (newMap);
			//storing the new map with smoothing changes into our current map
			map = newMap;
		}
		ProcessMap ();
	}


	void PopulateMap ()
	{
		//While I was not asked to use a seed in the assignment, I wanted to do so for my own testing. Being able to come back to the exact map to test if passageways are working and etc was extremely helpful.
		if (randomizeMapSeed) {
			//setting the range of the seed
			seed = UnityEngine.Random.Range (-1000, 1000).ToString ();
		}
		//creating a "random" number generator, and converting the seed into an integer
		System.Random rand = new System.Random (seed.GetHashCode ());
		//iterating through each tile in the map
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				//setting a border of walls for the map 
				if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
					map [x, y] = 1;
					//randomly assigning walls based on the users wall percentage
					//if the next "random" number is less than the wall percentage, then we want to add a wall in the map
					//otherwise if it is greater, leave it as a cave
				} else {
					map [x, y] = (rand.Next (0, 100) < wallPercent) ? 1 : 0;
				}
			}
		}
	}


	void SmoothMap (int[,] map)
	{
		//iterating through all of the map positions
		for (int x = 1; x < width - 1; x++) {
			for (int y = 1; y < height - 1; y++) {
				//getting the count of the walls surrounding each position
				int neighbourWallTiles = CalculateSurroundingWalls (x, y);
				//if the number of wall tiles surrounding the position is greater than 4, set that position to be a wall as well
				if (neighbourWallTiles > 4)
					map [x, y] = 1;
				//however if the number of walls surrounding that position is less than 4, leave it as a cave
				else if (neighbourWallTiles < 4)
					map [x, y] = 0;

			}
		}
	}


	int CalculateSurroundingWalls (int mapX, int mapY)
	{
		//starting the wall count at 0
		int numOfWalls = 0;
		//looping through a 3x3 grid, centered on the tile at mapX and mapY
		for (int neighbourX = mapX - 1; neighbourX <= mapX + 1; neighbourX++) {
			for (int neighbourY = mapY - 1; neighbourY <= mapY + 1; neighbourY++) {
				//making sure that we are not looking at our original tile
				if (neighbourX != mapX || neighbourY != mapY) {
					//if a neighbour is a wall, the numOfWalls will increase
					numOfWalls += map [neighbourX, neighbourY];
				}
			} 
		}
		return numOfWalls;
	}


	void OnDrawGizmos ()
	{
		if (map != null) {
			//for each of the tiles in the map, we're going to set their color based on whether they are a wall or cave tile
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					//I'm putting this in a try/catch as when users are creating larger maps OnDrawGizmos is attempting to visualize the larger map before users have generated it
					//While there is no serious issues, without it, errors will be thrown for indexes being out of range until the user clicks the editor window to generate the new map
					//this is simply for ease of use
					try {
						//if the value at map[x,y] is 1, it is a wall and will be colored black, otherwise it is part of the cave and will be white
						Gizmos.color = (map [x, y] == 1) ? Color.black : Color.white;
						//simply centering the map
						Vector3 pos = new Vector3 ((-width * 0.5f) + x + 0.5f, 0, (-height * 0.5f) + y + 0.5f);
						//drawing all of the cubes to represent the map
						Gizmos.DrawCube (pos, Vector3.one);
						
					} catch (Exception ex) {
						
					}
				}
			}
		}
	}

	//=======================================================================================================================================================================================================================//
	//This is where the tutorial code begins, I intend to study further about the flood-fill algorithm that has been used and how regions are connected via passageways.

	struct Coord
	{
		public int tileX;
		public int tileY;

		public Coord (int x, int y)
		{
			tileX = x;
			tileY = y;
		}
	}

	void ProcessMap ()
	{
		List<List<Coord>> wallRegions = GetRegions (1);
		int wallThresholdSize = 50;

		foreach (List<Coord> wallRegion in wallRegions) {
			if (wallRegion.Count < wallThresholdSize) {
				foreach (Coord tile in wallRegion) {
					map [tile.tileX, tile.tileY] = 0;
				}
			}
		}

		List<List<Coord>> roomRegions = GetRegions (0);
		int roomThresholdSize = 50;
		List<Room> survivingRooms = new List<Room> ();

		foreach (List<Coord> roomRegion in roomRegions) {
			if (roomRegion.Count < roomThresholdSize) {
				foreach (Coord tile in roomRegion) {
					map [tile.tileX, tile.tileY] = 1;
				}
			} else {
				survivingRooms.Add (new Room (roomRegion, map));
			}
		}
		survivingRooms.Sort ();
		survivingRooms [0].isMainRoom = true;
		survivingRooms [0].isAccessibleFromMainRoom = true;

		ConnectClosestRooms (survivingRooms);
	}

	void ConnectClosestRooms (List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
	{

		List<Room> roomListA = new List<Room> ();
		List<Room> roomListB = new List<Room> ();

		if (forceAccessibilityFromMainRoom) {
			foreach (Room room in allRooms) {
				if (room.isAccessibleFromMainRoom) {
					roomListB.Add (room);
				} else {
					roomListA.Add (room);
				}
			}
		} else {
			roomListA = allRooms;
			roomListB = allRooms;
		}

		int bestDistance = 0;
		Coord bestTileA = new Coord ();
		Coord bestTileB = new Coord ();
		Room bestRoomA = new Room ();
		Room bestRoomB = new Room ();
		bool possibleConnectionFound = false;

		foreach (Room roomA in roomListA) {
			if (!forceAccessibilityFromMainRoom) {
				possibleConnectionFound = false;
				if (roomA.connectedRooms.Count > 0) {
					continue;
				}
			}

			foreach (Room roomB in roomListB) {
				if (roomA == roomB || roomA.IsConnected (roomB)) {
					continue;
				}

				for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++) {
					for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++) {
						Coord tileA = roomA.edgeTiles [tileIndexA];
						Coord tileB = roomB.edgeTiles [tileIndexB];
						int distanceBetweenRooms = (int)(Mathf.Pow (tileA.tileX - tileB.tileX, 2) + Mathf.Pow (tileA.tileY - tileB.tileY, 2));

						if (distanceBetweenRooms < bestDistance || !possibleConnectionFound) {
							bestDistance = distanceBetweenRooms;
							possibleConnectionFound = true;
							bestTileA = tileA;
							bestTileB = tileB;
							bestRoomA = roomA;
							bestRoomB = roomB;
						}
					}
				}
			}
			if (possibleConnectionFound && !forceAccessibilityFromMainRoom) {
				CreatePassage (bestRoomA, bestRoomB, bestTileA, bestTileB);
			}
		}

		if (possibleConnectionFound && forceAccessibilityFromMainRoom) {
			CreatePassage (bestRoomA, bestRoomB, bestTileA, bestTileB);
			ConnectClosestRooms (allRooms, true);
		}

		if (!forceAccessibilityFromMainRoom) {
			ConnectClosestRooms (allRooms, true);
		}
	}

	void CreatePassage (Room roomA, Room roomB, Coord tileA, Coord tileB)
	{
		Room.ConnectRooms (roomA, roomB);
		Debug.DrawLine (CoordToWorldPoint (tileA), CoordToWorldPoint (tileB), Color.green, 100);

		List<Coord> line = GetLine (tileA, tileB);
		foreach (Coord c in line) {
			DrawCircle (c, 5);
		}
	}

	void DrawCircle (Coord c, int r)
	{
		for (int x = -r; x <= r; x++) {
			for (int y = -r; y <= r; y++) {
				if (x * x + y * y <= r * r) {
					int drawX = c.tileX + x;
					int drawY = c.tileY + y;
					if (IsInMapRange (drawX, drawY)) {
						map [drawX, drawY] = 0;
					}
				}
			}
		}
	}

	List<Coord> GetLine (Coord from, Coord to)
	{
		List<Coord> line = new List<Coord> ();

		int x = from.tileX;
		int y = from.tileY;

		int dx = to.tileX - from.tileX;
		int dy = to.tileY - from.tileY;

		bool inverted = false;
		int step = Math.Sign (dx);
		int gradientStep = Math.Sign (dy);

		int longest = Mathf.Abs (dx);
		int shortest = Mathf.Abs (dy);

		if (longest < shortest) {
			inverted = true;
			longest = Mathf.Abs (dy);
			shortest = Mathf.Abs (dx);

			step = Math.Sign (dy);
			gradientStep = Math.Sign (dx);
		}

		int gradientAccumulation = longest / 2;
		for (int i = 0; i < longest; i++) {
			line.Add (new Coord (x, y));

			if (inverted) {
				y += step;
			} else {
				x += step;
			}

			gradientAccumulation += shortest;
			if (gradientAccumulation >= longest) {
				if (inverted) {
					x += gradientStep;
				} else {
					y += gradientStep;
				}
				gradientAccumulation -= longest;
			}
		}

		return line;
	}

	Vector3 CoordToWorldPoint (Coord tile)
	{
		return new Vector3 (-width / 2 + .5f + tile.tileX, 2, -height / 2 + .5f + tile.tileY);
	}

	List<List<Coord>> GetRegions (int tileType)
	{
		List<List<Coord>> regions = new List<List<Coord>> ();
		int[,] mapFlags = new int[width, height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (mapFlags [x, y] == 0 && map [x, y] == tileType) {
					List<Coord> newRegion = GetRegionTiles (x, y);
					regions.Add (newRegion);

					foreach (Coord tile in newRegion) {
						mapFlags [tile.tileX, tile.tileY] = 1;
					}
				}
			}
		}

		return regions;
	}

	List<Coord> GetRegionTiles (int startX, int startY)
	{
		List<Coord> tiles = new List<Coord> ();
		int[,] mapFlags = new int[width, height];
		int tileType = map [startX, startY];

		Queue<Coord> queue = new Queue<Coord> ();
		queue.Enqueue (new Coord (startX, startY));
		mapFlags [startX, startY] = 1;

		while (queue.Count > 0) {
			Coord tile = queue.Dequeue ();
			tiles.Add (tile);

			for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
				for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
					if (IsInMapRange (x, y) && (y == tile.tileY || x == tile.tileX)) {
						if (mapFlags [x, y] == 0 && map [x, y] == tileType) {
							mapFlags [x, y] = 1;
							queue.Enqueue (new Coord (x, y));
						}
					}
				}
			}
		}

		return tiles;
	}

	bool IsInMapRange (int x, int y)
	{
		return x >= 0 && x < width && y >= 0 && y < height;
	}

	class Room : IComparable<Room>
	{
		public List<Coord> tiles;
		public List<Coord> edgeTiles;
		public List<Room> connectedRooms;
		public int roomSize;
		public bool isAccessibleFromMainRoom;
		public bool isMainRoom;

		public Room ()
		{
		}

		public Room (List<Coord> roomTiles, int[,] map)
		{
			tiles = roomTiles;
			roomSize = tiles.Count;
			connectedRooms = new List<Room> ();

			edgeTiles = new List<Coord> ();
			foreach (Coord tile in tiles) {
				for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
					for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
						if (x == tile.tileX || y == tile.tileY) {
							if (map [x, y] == 1) {
								edgeTiles.Add (tile);
							}
						}
					}
				}
			}
		}

		public void SetAccessibleFromMainRoom ()
		{
			if (!isAccessibleFromMainRoom) {
				isAccessibleFromMainRoom = true;
				foreach (Room connectedRoom in connectedRooms) {
					connectedRoom.SetAccessibleFromMainRoom ();
				}
			}
		}

		public static void ConnectRooms (Room roomA, Room roomB)
		{
			if (roomA.isAccessibleFromMainRoom) {
				roomB.SetAccessibleFromMainRoom ();
			} else if (roomB.isAccessibleFromMainRoom) {
				roomA.SetAccessibleFromMainRoom ();
			}
			roomA.connectedRooms.Add (roomB);
			roomB.connectedRooms.Add (roomA);
		}

		public bool IsConnected (Room otherRoom)
		{
			return connectedRooms.Contains (otherRoom);
		}

		public int CompareTo (Room otherRoom)
		{
			return otherRoom.roomSize.CompareTo (roomSize);
		}
	}
}