﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

[CustomEditor (typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
	
	public override void OnInspectorGUI ()
	{
		//Setting the target
		MapGenerator mapGenerator = (MapGenerator)target;
		//Setting the target to dirty allows for the editor to be redrawn/refreshed, which is useful as everything is being done in editor mode
		EditorUtility.SetDirty (target);
		//Drawing the default inspector so that users can input their values
		DrawDefaultInspector ();

		//Inspector button that allows users to generate a new map using their specifications. 
		//This uses the existing values users have entered and creates a new map around them. Essentially a new "layout" for the cave.
		if (GUILayout.Button ("Generate New Cave")) {
			mapGenerator.GenerateMap ();
		}

		//Inspector button that will allow users to save the newly created map
		if (GUILayout.Button ("Save Map")) {
			//checking if the directory for the prefabs exist
			if (!Directory.Exists ("Assets/MapPrefabs/")) {    
				//if it doesn't, create it
				Directory.CreateDirectory ("Assets/MapPrefabs/");		
			}
			//Gathering all the transforms in the selected map
			Transform[] transforms = Selection.transforms;
			//Iterating through each and saving them into a new prefab
			foreach (Transform t in transforms) {
				//The name of the map will be used for the new prefab, users can input their own name for the map prefab.
				Object prefab = PrefabUtility.CreateEmptyPrefab ("Assets/MapPrefabs/" + mapGenerator.name + ".prefab");
				//Existing prefabs with the same name will be replaced with the new map
				PrefabUtility.ReplacePrefab (t.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
			}
		}
	}

}
