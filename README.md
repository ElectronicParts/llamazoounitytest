Brett Dressler
November 09, 2016
LlamaZOO Unity Technical Test Readme

Operation instructions:

	- Download project, or import and apply the unity package to a new project.

	- As requested, everything is done within the editor mode, and is configured using the MapGenerator GameObject in the scene.

	- Attached to this object is the MapGenerator script which will allow users to configure their map to their liking

	- Users can choose their desired width and height, and the percent of the map that will be walls
		- I have found that 50% creates the nicest looking caves, however please feel free to play around with this to your liking. A lower percent will generate more open caves, whereas a higher percent will generate more narrow and smaller caves.
	
	- A name is required for each map, as it is needed for saving the map prefabs. If you do not provide one, it will not save the map and will throw and error
		- Maps saved as prefabs can also be overwritten and updated if the name is not changed when you save a new map. 
		- For example, a user can create a cave that is 100 x 100 and save it as "Cave 1" and then go back and update the size or wall percentage, and save it as "Cave 1" again and it will update the existing prefab rather than making a copy.
	
	- I have provided the optional feature of using a seed instead of randomizing the cave itself, this was very helpful in my testing being able to go back to the same map again and again rather than a new random one each time.
		- By default, randomizeMapSeed is checked to true, and will allow for users to generate different maps again and again, however if unchecked in the inspector, users can now create custom seeds that will generate the same maps again and again.
	
	- Once all values have been input by the user, clicking on the "Generate New Cave" button in the inspector will do just that. A map will be created in the desired width and height, and will be filled with walls based on the user's chosen wall percentage.
	
	- If the user doesn't like the layout of the cave that was generated, the user can simply click "Generate New Cave" to create another with the given parameters.
		- Note: This will not work when using a custom seed
	
	- When the user is satisfied with their map, they can click "Save Map" in the inspector and a folder will be created in the Assets directory if it is not already present, with a prefab of the map with the same parameters given by the user, and is named based on what the user chose for a name earlier.


Personal thoughts and notes:
	
	I actually really enjoyed working on this project, and I had the opportunity to learn a lot in the implementation. I previously had worked with Cellular Automata while at VFS however it had been some time, and this gave me a great opportunity to expand my knowledge.
	I find it extremely interesting that I can now create procedural levels, and see a number of possibilities of using this in the future, whether at LlamaZOO or on my own personal projects. I personally only used 2 states in this project, the map can either contain a wall or a cave tile (the absence of a wall).

	I would like to note that due to time constraints, I used parts of this tutorial as refrence for the connection of rooms during this project, (https://www.youtube.com/watch?v=eVb9kQXvEZM). I was able to integrate it successfully with code that I had already written.
	I wanted to be transparent and to let you know that I needed to research how to accomplish this particular piece, and found the need to study the code further to better understand it.
	I have created a section within the code to show where the code from the tutorial begins. I hope this shows my skill at researching solutions to problems, and implementing and adapting other's code to work within an existing code base.
   
	Completing this assignment was challenging at times but also a lot of fun. If this is just a taste of some of the things that potential hires would be doing at LlamaZOO, I look forward to seeing what else I can work with!